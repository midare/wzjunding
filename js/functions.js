$(document).ready(function(){

    var H_BLANK = 2000;
    var CT_WIDTH = 960;
    var P_TOP = 0;
    var cNav;
    var aHeight;

    function getLeft() {
        return Math.round((document.body.clientWidth - CT_WIDTH) / 2);
    }

    function fixLeft() {
        var l = getLeft();
        $('.sub>.row').each(function(index,element){
            $(element).children('.container').each(function(index,element){
                $(element).css({'left': l + (index ? index : 0 ) * H_BLANK});
            })
        })
        if ( aHeight==undefined ) {
            if ( window.innerHeight > 650 ) {
                aHeight = window.innerHeight;
                $('.sub').each(function(index,element){
                    $(element).height(aHeight - P_TOP).css({'top': index * aHeight})
                })
            } else {
                aHeight = 650;
                $('.sub').each(function(index,element){
                    $(element).height(650 - P_TOP).css({'top': index * 650 })
                })
            }
        } else {
            if ( window.innerHeight > 650 ) {
                $('.sub').each(function(index,element){
                    var m = $(element).position().top / aHeight;
                    $(element).height(window.innerHeight - P_TOP).css({'top': m * window.innerHeight})
                })
                aHeight = window.innerHeight;
            } else {
                $('.sub').each(function(index,element){
                    var m = $(element).position().top / aHeight;
                    $(element).height(650 - P_TOP).css({'top': m * 650 })
                })
                aHeight = 650;
            }
            
        }
        resetDirection();
    }

    function resetDirection() {
        var left = getLeft()
        if ( cNav==undefined ) {
            $('.direction-left').css({'left': left - 40});
            $('.direction-right').css({'left': left + 40 + CT_WIDTH - $('.direction-left').width()})
        } else {
            var l = cNav.children().first()
            var r = cNav.children().last()
            var lm = Math.floor(l.position().left / H_BLANK);
            var rm = Math.floor(r.position().left / H_BLANK);
            l.css({'left':left - 40 + lm * H_BLANK});
            r.css({'left':left + 40 - l.width() + CT_WIDTH + rm * H_BLANK});
        }
    }

    $('.sub').not('#mainpage').not('#contact').append(
        $('<div>').addClass('direction-nav').append(
            $('<a>').attr({"href":"#"}).addClass('direction-left').text('←')
        ).append(
            $('<a>').attr({"href":"#"}).addClass('direction-right').text('→')
        )
    ).first().children('.direction-nav').show();

    fixLeft();
    $(window).resize(fixLeft);

    $('.direction-left').click(function(){
        var sub = $(this).parents('.sub');
        if(sub.is(':animated')){
            return;
        }
        cNav = $(this).parent();
        var r = sub.children('.row');
        var l = r.offset().left;
        var size = r.children('.container').size()
        if ( l ){
            sub.animate({'left':'+=' + H_BLANK});
            $(this).add($(this).next()).animate({'left':'-=' + H_BLANK});
        } else {
            sub.animate({'left': -(l + (size-1)*H_BLANK) });
            $(this).add($(this).next()).animate({'left':'+=' + (size-1) * H_BLANK });
        }
    })

    $('.direction-right').click(function(){
        var sub = $(this).parents('.sub');
        if(sub.is(':animated')){
            return;
        }
        cNav = $(this).parent();
        var r = sub.children('.row');
        var l = sub.offset().left;
        if ( l + (r.children('.container').size()-1)*H_BLANK ){
            sub.animate({'left':'-=' + H_BLANK});
            $(this).add($(this).prev()).animate({'left':'+=' + H_BLANK});
        } else {
            sub.animate({'left':'0'});
            $(this).add($(this).prev()).animate({'left':'+=' + l });
        }
    })

    $('#sidenav nav>a').children('a:first').css('color','white');
    $('#sidenav nav>a').children('a:first').addClass('active');
    $('#sidenav nav>a').each(function(index,element){

        $(this).mouseenter(function(){
            if ( !$(this).hasClass('active') ){
                $(this).animate({color:'#FFF'},'fast');
            }
        });

        $(this).mouseleave(function(){
            if ( !$(this).hasClass('active') ){
                $(this).animate({color:'#b3a36b'},'fast');
            }
        });

        $(this).click(function(){
            $('#side div').delay(400).fadeOut();
            $('#topnav').delay(1000).fadeIn();
            if($('.sub').is(':animated')){
                return;
            }
            var offset = 0 - $('.sub:eq('+index+')').position().top;
            if ( offset != 0 ) {
                $('.sub').each(function(index,element){
                    $(element).stop().animate({top:$(element).position().top + offset},1800,function(){
                        $('.sub').css({'left':0});
                        cNav = undefined;
                        resetDirection();
                    });
                });
            }
        })
    })
    $(".navtomain").click(function(){
        $('#side div').delay(500).fadeIn();
        $("#topnav").fadeOut();
    });
    $('#topnav nav>a').each(function(index,element){

        $(this).click(function(){
            if($('.sub').is(':animated')){
                return;
            }
            var offset = 0 - $('.sub:eq('+index+')').position().top;
            if ( offset != 0 ) {
                $('.sub').each(function(index,element){
                    $(element).stop().animate({top:$(element).position().top + offset},1800,function(){
                        $('.sub').css({'left':0});
                        cNav = undefined;
                        resetDirection();
                    });
                });
            }
        })
    })

    //gallery

    $('.gallery').galleryView({
        filmstrip_position: 'right',
        frame_height: 32,
        frame_width: 50,
        panel_height: 400,
        panel_animation: 'crossfade',
        enable_overlays: true,
        show_infobar: false,
        enable_slideshow: false
    });

    //baidu map

    var sContent =
    "<h4 style='margin:0 0 5px 0;padding:0.2em 0'>中粮君顶华悦(温州)俱乐部</h4>" +
    "<p style='margin:0;line-height:1.5;font-size:13px'>温州市鹿城区东明路222号（瓯江路中端曼哈顿东区12幢）</p>";
    var map = new BMap.Map("map");
    var point = new BMap.Point(120.679916,28.027859);
    var marker = new BMap.Marker(point);
    var infoWindow = new BMap.InfoWindow(sContent);  // 创建信息窗口对象
    map.addControl(new BMap.NavigationControl());
    map.centerAndZoom(point, 16);
    map.addOverlay(marker);
    marker.openInfoWindow(infoWindow);
    marker.addEventListener("click", function(){
       this.openInfoWindow(infoWindow);
    });
})